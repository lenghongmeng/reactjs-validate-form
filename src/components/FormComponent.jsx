import React, { Component } from 'react'
import * as Yup from "yup"
import { Formik, validateYupSchema, Field, Form, ErrorMessage } from 'formik';
import HomeComponent from './HomeComponent';

const LoginSchema = Yup.object().shape({
  email: Yup.string()
    .email("Invalid email format")
    .required("Email is required"),

  password: Yup.string()
    .min(6, "Password should be more than 6 characters")
    .required("Password cannot be empty"),

  username: Yup.string()
    .min(7, "Username should be 8 characters up")
    .required("You must enter a username"),
});

export default class FormComponent extends Component {

  render() {
    return (
      <div>
        <div className='w-full flex'>
          <div className='w-1/3 bg-cover bg-[url("https://images.pexels.com/photos/114979/pexels-photo-114979.jpeg?cs=srgb&dl=pexels-veeterzy-114979.jpg&fm=jpg")]'>

          </div>
          <div className='w-2/3 flex flex-col h-screen justify-center items-center'>
            <div className='border shadow-md rounded-lg border-slate-300 py-12 m-auto w-2/4'>
              <p className='text-3xl font-bold'>Login Form</p>
              <br />

              <Formik
                initialValues={{ email: "", password: "", username: "" }}
                validationSchema={LoginSchema}
              >
                {
                  ({ isSubmitting }) => !isSubmitting ? (
                    <Form action="">
                      <label className='block w-2/3 m-auto text-left'>Email Addres</label>
                      <Field type="email" name="email" placeholder="Enter your email" class="border border-slate-300 rounded-md w-2/3 py-2 px-3 m-2 focus:outline-none focus:ring focus:ring-violet-300" />
                      <div className='h-2 mb-4 mt-[-5px]'>
                        <ErrorMessage component="div" name="email" className="block w-2/3 m-auto text-left mb-2 text-red-600 text-sm" />
                      </div>

                      <label className='block w-2/3 m-auto text-left'>Username</label>
                      <Field type="username" name="username" placeholder="Enter your username" class="border border-slate-300 rounded-md w-2/3 py-2 px-3 m-2 focus:outline-none focus:ring focus:ring-violet-300" />
                      <div className='h-2 mb-4 mt-[-5px]'>
                        <ErrorMessage component="div" name="username" className="block w-2/3 m-auto text-left mb-2 text-red-600 text-sm" />
                      </div>


                      <label className='block w-2/3 m-auto text-left'>Password</label>
                      <Field type="password" name="password" placeholder="Enter your password" class="border border-slate-300 rounded-md w-2/3 py-2 px-3 m-2 focus:outline-none focus:ring focus:ring-violet-300" />
                      <div className='h-2 mb-4 mt-[-5px]'>
                        <ErrorMessage component="div" name="password" className="block w-2/3 m-auto text-left mb-2 text-red-600 text-sm" />
                      </div>
                      <button class="w-2/3 py-2 px-3 m-3 rounded-md font-bold text-white bg-violet-500 hover:bg-violet-600 active:bg-violet-700 focus:outline-none focus:ring focus:ring-violet-300">
                        Submit
                      </button>
                    </Form>
                  ) : (
                    <HomeComponent />
                  )
                }
              </Formik>
            </div>
          </div>

        </div >
      </div >
    )
  }
}
